#!/bin/bash

install(){
	echo "installing the package $1..."
	apt-get -y install $1
}
runlevel(){
	if [ "$1" == "network-manager" ]
	then
		sysv-rc-conf --level 2345 $1 on
	fi
}
pckg_check(){
	dpkg -s "$1" > /dev/null 2>&1 && {
		echo "package $1 is already installed."
		runlevel $1
	} || {
		echo "package $1 is not installed."
		install $1
		runlevel $1
	}
}
#pckg_check sysv-rc-conf
#pckg_check network-manager
#service networking restart
#service network-manager restart
#route add -net 192.168.1.0 netmask 255.255.255.0 gw 192.168.1.1 dev p4p1
sysctl -w net.ipv4.ip_forward=1
net_interface(){
	#. $1 = p4p1 or em1 (the ethernet interface)
	echo "auto $1" >> /etc/network/interfaces
	echo "iface $1 inet static" >> /etc/network/interfaces
	echo "address 192.168.1.2" >> /etc/network/interfaces
	echo "netmask 255.255.255.0" >> /etc/network/interfaces
	echo "gateway 192.168.1.1" >> /etc/network/interfaces
}
firewall(){
	dongle_ip="$(ip addr show eth0 | grep -Po 'inet \K[\d.]+')"
	iptables -t nat -A POSTROUTING -o eth0 -j SNAT --to $dongle_ip
	iptables -I INPUT -i lo -j ACCEPT
	#iptables -I INPUT -i em1 -j ACCEPT
	#iptables -I INPUT -i eth0 -j ACCEPT
	iptables -A OUTPUT -p udp --dport 53 -j ACCEPT
	iptables -A OUTPUT -p tcp -d play.google.com,store.apple.com -dport 443 -j ACCEPT
	iptables -A INPUT -m conntrack --cstate ESTABLISHED,RELATED -j ACCEPT
	iptables -P INPUT DROP
	iptables -P OUTPUT DROP 
	service ufw restart
}
firewall
iptables -t nat -A POSTROUTING -o eth0 -p tcp -d play.google.com,store.google.com -j SNAT --to 192.168.8.100
